import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.css'],
})
export class PropertyListComponent implements OnInit {
  properties: Array<any> = [
    {
      Id: 1,
      Name: 'Targoviste Bens',
      Type: 'House',
      Price: 12000,
    },
    {
      Id: 2,
      Name: 'Bucuresti Homes',
      Type: 'Apartments',
      Price: 75000,
    },
    {
      Id: 3,
      Name: 'Cluj Bens',
      Type: 'House',
      Price: 34000,
    },
    {
      Id: 4,
      Name: 'Targoviste Suits',
      Type: 'Apartments',
      Price: 11000,
    },
    {
      Id: 5,
      Name: 'Sibiu Homes',
      Type: 'House',
      Price: 75000,
    },
    {
      Id: 6,
      Name: 'Bucuresti Penth',
      Type: 'Penthouse',
      Price: 100000,
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
